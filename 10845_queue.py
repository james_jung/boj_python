import sys
from collections import deque

n = int(input())
l = deque([])
for _ in range(n):
    cmd = sys.stdin.readline().split()

    if cmd[0] == 'push':
        l.append(cmd[1])
    elif cmd[0] == 'pop':
        if len(l) > 0:
            print(l[0])
            l.popleft()
        else:
            print(-1)
    elif cmd[0] == 'size':
        print(len(l))
    elif cmd[0] == 'empty':
        if len(l) != 0:
            print(0)
        else:
            print(1)
    elif cmd[0] == 'front':
        if len(l) == 0:
            print(-1)
        else:
            print(l[0])
    elif cmd[0] == 'back':
        if len(l) == 0:
            print(-1)
        else:
            print(l[-1])

