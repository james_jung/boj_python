# # test cases
t = int(input())

def fibonacci(n):

    if n == 0:
        return int(0)
    elif n == 1:
        return int(1)
    else:
        return fibonacci(n-1) + fibonacci(n-2)



for _ in range(t):
    a = int(input())
    cnt_0 = 0
    cnt_1 = 0
    while a >= 0:
        for i in range(a+1):
            if fibonacci(i) == 0:
                cnt_0 += 1
            elif fibonacci(i) == 1:
                cnt_1 += 1
        print('0: ', cnt_0,'1: ', cnt_1)
        a -= 1

    # print(cnt_0, cnt_1)
