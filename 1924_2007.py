x, y = map(int, input().split())

day_list = ['SUN','MON','TUE','WED','THU','FRI','SAT']
days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

cnt = 0
for i in range(x-1):
    cnt += days[i]

total_days = cnt + y
d = total_days % 7

print(day_list[d])



