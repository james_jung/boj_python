import sys

n = int(input())
l = []

for _ in range(n):
    cmd = sys.stdin.readline().rstrip().split()

    if cmd[0] == 'push':
        l.append(cmd[1])
    elif cmd[0] == 'pop':
        if len(l) != 0:
            print(l[-1])
            l.pop()
        else:
            print(-1)
    elif cmd[0] == 'size':
        print(len(l))
    elif cmd[0] == 'empty':
        if len(l) != 0:
            print(0)
        else:
            print(1)
    elif cmd[0] == 'top':
        if len(l) != 0:
            print(l[-1])
        else:
            print(-1)