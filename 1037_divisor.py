n = int(input())
data = list(map(int, input().split()))

# 2, 4, 8
# 결과는 16

# 3
# 결과는 9

# 3, 9
# 결과는 27

# 2, 3, 4, 6
# 결과는 12

result = min(data) * max(data)
print(result)

