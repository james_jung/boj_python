n, k = map(int, input().split())
circle_list = [i for i in range(1, n+1)]
answer = []
pop_num = 0

while circle_list:
    pop_num = (pop_num + (k-1)) % len(circle_list)
    pop_element = circle_list.pop(pop_num)
    answer.append(str(pop_element))

print("<%s>" %(", ".join(answer)))