import sys

a, b = map(int, sys.stdin.readline().split())
c = a * b

while b != 0:
    a = a % b
    if a < b:
        a, b = b, a

print(a)
print(c//a)