import sys
import heapq

n = int(input())
heap = []

for _ in range(n):
    a = int(sys.stdin.readline())
    if a != 0:
        heapq.heappush(heap, a)
    else:
        try:
            print(heapq.heappop(heap))
        except:
            print(0)
