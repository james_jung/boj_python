n = int(input())
name_list = [list(input()) for i in range(n)]

for i in range(1, n):
    for j in range(len(name_list[0])):
        if name_list[0][j] != name_list[i][j]:
            name_list[0][j] = '?'

print(''.join(name_list[0]))

# 문자 길이는 같으니
# 첫번째 문자부터 loop를 순회하면서
# 같으면 문자를 유지하고
# 다르면 ?를 넣는다
# 근데 셋 다 .인 경우에는 .으로 한다