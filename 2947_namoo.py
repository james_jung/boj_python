data = list(map(int, input().split()))

while data != [1,2,3,4,5]:
    for i in range(len(data)-1):
        if data[i] > data[i+1]:
            data[i], data[i+1] = data[i+1], data[i]
            print(' '.join(map(str, data)))
