n = int(input())
data = list(map(int, input().split()))

m, y = 0, 0
for d in data:
    # y는 30초마다 10원 청구
    # m은 60초마다 15원 청구
    y += 10 + (d//30) * 10
    m += 15 + (d//60) * 15


if y > m:
    print('M', m)
elif y < m:
    print('Y', y)
else:
    print('Y', 'M', y)