import heapq

n = int(input())
heap = []
heapq.heapify(heap)
answer = 0

for _ in range(n):
    a = int(input())
    heapq.heappush(heap,a)

while heap:
    answer += heapq.heappop(heap)


print(answer)